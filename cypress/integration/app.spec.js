/// <reference types="Cypress" />

import { MainPage } from '../page/MainPage';

context('When user on index page', function() {
  let page;

  beforeEach(() => {
    page = new MainPage();
    page.visit;
  });

  it('Should be able successfully load the index page', function() {
    cy.get('h1').should('be.visible');
    page.ensure;
  });

  it('Should be able to see 4 containers', function() {
    page.containers
      .should('be.visible')
      .should('have.length', 4);
  });

  it('Should be able to fill containers with bottles', function() {
    page.containers
      .should('be.visible');

    page.noBottles.should('be.visible');
    page.shuffleButton.click();

    page.noBottles.should('not.be.visible');
    cy.get('[data-test-id=container]').first().find('[class^=Bottle__bottle]').should('have.length', 3);
  });

  it('Should see the sensor values', function() {

    page.sensors.should('not.exist');
    page.shuffleButton.click();

    page.sensors
      .should('have.length', 4);

    page.sensors
      .first()
      .should('not.be.empty');
  });

  it('Should get appropriate alerts', function() {
    page.alerts.should('not.exist');

    page.shuffleButton.click();

    // Mocked sensors have following value: -1, 5, 9, undefined so it's a set of Alert, Ok, Alert, No alert states.

    page.sensors
      .should('have.length', 4);

    page.alerts
      .should('have.length', 3);

    page.sensors.eq(0).contains('-1');
    page.alerts.eq(0).find('span').contains('Alert!');

    page.sensors.eq(1).contains('5');
    page.alerts.eq(1).find('span').contains('Ok!');

    page.sensors.eq(2).contains('9');
    page.alerts.eq(2).find('span').contains('Alert!');

    page.sensors.eq(3).contains('---');
    page.alerts.eq(3).should('not.exist');
  });
});
