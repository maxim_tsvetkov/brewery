export class MainPage {
  get visit() {
    return cy.visit('http://localhost:8080');
  }

  get ensure() {
    return cy.get('h1').contains('Shane\'s Brewery');
  }

  get containers() {
    return cy.get('[data-test-id=container]');
  }

  get sensors() {
    return cy.get('[data-test-id=sensor]');
  }

  get alerts() {
    return cy.get('[data-test-id=alert]');
  }

  get noBottles() {
    return cy.get('h4').contains('No bottles!');
  }

  get shuffleButton() {
    return cy.get('[data-test-id=shuffleButton]');
  }
}
