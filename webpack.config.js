const path = require('path');
const basePath = __dirname;

const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
    mode: 'production',
    context: path.join(basePath, 'src'),
    resolve: {
        extensions: ['.js', '.jsx', '.css', '.scss'],
    },
    entry: './index.js',
    output: {
        path: path.resolve(basePath, 'dist'),
        filename: '[name].[hash].js',
        hashDigestLength: 7,
    },
    devtool: false,
    devServer: {
        inline: true,
        host: 'localhost',
        port: 8080,
        publicPath: '/',
    },
    performance: { hints: false },
    module: {
        rules: [
            {test: /\.js$/, use: 'babel-loader'},
            {test: /\.css$/, include: /node_modules/, use: [MiniCssExtractPlugin.loader, 'css-loader']},
            {
                test: /\.css$/, exclude: /node_modules/, use: [
                    MiniCssExtractPlugin.loader,
                    {
                        loader: 'css-loader',
                        options: {
                            modules: {
                                localIdentName: `[name]__[local]__[hash:base64:7]`,
                            },
                            localsConvention: 'camelCase',
                        },
                    },
                ]
            },
            {
                test: /\.scss$/, exclude: /node_modules/, use: [
                    MiniCssExtractPlugin.loader,
                    {
                        loader: 'css-loader',
                        options: {
                            modules: {
                                localIdentName: `[name]__[local]__[hash:base64:7]`,
                            },
                            localsConvention: 'camelCase',
                        },
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            sourceMap: false,
                        },
                    },
                ]
            },
            {
                test: /\.(png)$/,
                loader: 'file-loader',
                options: {
                    name: `[name].[hash:7].[ext]`,
                    outputPath: 'images',
                },
            },
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            filename: path.join(basePath, 'dist/index.html'),
            template: 'index.html',
            favicon: './favicon.ico',
        }),
        new MiniCssExtractPlugin({
            filename: `[name].[hash:7].css`,
            chunkFilename: `[id].[hash:7].css`,
        }),
    ],
};
