# Shane's Brewery

## Quick start

- run application
``` bash 
npm install
npm run server
```

- run tests
``` bash
npm run server:test
npm run test:e2e
npm run open:coverage
```

#### Technical set (built by myself):
- [React](https://github.com/facebook/react)
- [Sass](https://github.com/sass/sass)
- [Webpack](https://webpack.js.org/)
- [Babel](https://babeljs.io/)
- [Prettier](https://prettier.io/), [Eslint](https://eslint.org/)
- [Cypress](https://www.cypress.io)


#### Current unit test coverage

| % Stmts | % Branch | % Funcs | % Lines |

|   74.73 |    66.67 |   44.83 |   74.71 |

Great article about TDD: [article](https://www.toptal.com/react/tdd-react-unit-testing-enzyme-jest) which can be used as a manual on regular basic.

#### Current e2e coverage

- 95% Statements 95/100
- 76.6% Branches 36/47
- 90.32% Functions 28/31
- 94.79% Lines 91/96


#### Key Point Summary

- The refrigerated truck has 4 containers
- Every container can be filled with bottles randomly (through 'Shuffle' button)
- Shane should be aware by system in a case when all bottles in the refrigerator are in danger

#### Technical details

Custom webpack setup, a mix of eslintner and Prettier in order to improve a code quality.

Client is based on React with Hooks approach. Components have been implemented in functional way.
The main motivation is available [here](https://reactjs.org/docs/hooks-intro.html). 

One more advantage is we don't even need a redux library like a third-pert dependency.
There are two contexts (AppContext, SocketContext) in the App. That's it :)

#### How it works

Client (Web app) <---> Websocket server (Simple express server) <---> External Api (a sensor endpoint from Pragma)
   
Description: Websocket server provides data between a client and an external api through ws messages every 5 seconds (defined by TIME_POLLING_INTERVAL_SEC).

#### What could be better(TBD)

- increase code coverage in accordance with following [article](https://www.cypress.io/blog/2019/09/05/cypress-code-coverage-for-create-react-app-v3/),
current coverage is available after npm run test:e2e && npm run open:coverage;
- improve server/wsserver.js Create a separate node application;  
- start using Typescript or prop-types or Flow;
- prepare scripts for CI & CD. 

-----------------------------------------------------------------------------------

#### CLI Commands

``` bash
# install dependencies
npm install

# run ws-server and client on localhost:8080
npm run server

# run test ws-server and client on localhost:8080
npm run server:test

# build for production with minification
npm run build

# run tests with cypress in browser mode
npm run cypress

# run tests with cypress in console mode
npm run test:e2e

# run unit tests
npm run test

# open a code coverage report
npm run open:coverage
```

If some process is already started on port 8999 use: 
```bash
npm run server:stop
```
