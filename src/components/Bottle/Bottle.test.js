import React from 'react';
import { shallow } from 'enzyme';

import Bottle from './Bottle';
import { BeerTypes } from '../../mockData/data';

describe('Bottle component', () => {
  const pilsnerBottle = BeerTypes[0];
  let bottle;

  it('should render Bottle component', () => {
    bottle = shallow(<Bottle bottle={pilsnerBottle} />);
  });

  it('should render initial layout', () => {
    bottle = shallow(<Bottle bottle={pilsnerBottle} inDanger={false} />);
    expect(bottle.getElements()).toMatchSnapshot('./__snapshots__/Bottle.test.js.snap');
  });

  it('should render corner case layout', () => {
    const undefinedBottle = {
      name: null,
      min: undefined,
      max: undefined,
    };
    bottle = shallow(<Bottle bottle={undefinedBottle} inDanger={true} />);
    expect(bottle.getElements()).toMatchSnapshot('./__snapshots__/UndefinedBottle.test.js.snap');
  });
});
