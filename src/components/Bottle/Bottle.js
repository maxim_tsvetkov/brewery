import React from 'react';
import bottleImg from '../../assets/images/beer-bottle.png';
import styles from './Bottle.scss';

const Bottle = ({ bottle, inDanger }) => {
  const { name = 'Unknown', min = '---', max = '---' } = bottle;

  return (
    <div className={`${styles.bottle} ${inDanger ? styles.danger : ''}`}>
      <div className={styles.details}>
        <h4>{name}</h4>
        <p>{`${min}°C - ${max}°C`}</p>
      </div>
      <img src={bottleImg} alt={name} title={`${name} bottle`} width="40px" height="40px" />
    </div>
  );
};

export default Bottle;
