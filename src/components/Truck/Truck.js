import React, { useContext, useEffect } from 'react';
import ContainerCard from '../ContainerCard/ContainerCard';
import { AppContext } from '../../contexts/AppContext';
import { SocketContext, socketRunPolling } from '../../contexts/SocketContext';
import styles from './Truck.scss';

const Truck = () => {
  const appContext = useContext(AppContext);
  const { dispatch } = appContext;
  const containers = (appContext && appContext.state && appContext.state.containers) || [];
  const ids = containers ? containers.map((container) => container.id).join(',') : '';
  const socketContext = useContext(SocketContext);

  useEffect(() => {
    if (socketContext && socketContext.data) {
      const { id, temperature } = socketContext.data;
      dispatch({ type: 'UPDATE_TEMPERATURE', id, temperature });
    }
  }, [socketContext]);

  const onShuffleClick = () => {
    dispatch({ type: 'SHUFFLE_BOTTLES' });
    socketRunPolling({ ids });
  };

  return (
    <div className={styles.truck}>
      <div className={styles.header}>
        <h3>There is a beauty truck with 4 containers.</h3>
        <p>
          <button onClick={onShuffleClick} data-test-id="shuffleButton">
            Shuffle
          </button>
          <small>
            Click this button to fill containers with bottles. And press any time when you want to shuffle sorts of beer
            in containers!
          </small>
        </p>
      </div>
      <hr />
      <div className={styles.refrigerator}>
        {containers.length > 0 &&
          containers.map((container, index) => <ContainerCard container={container} key={index} />)}
      </div>
    </div>
  );
};

export default Truck;
