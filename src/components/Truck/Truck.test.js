import React from 'react';
import { shallow } from 'enzyme';

import Truck from './Truck';
import { initialContainers } from '../../mockData/data';
import { AppContext, AppContextProvider } from '../../contexts/AppContext';
import { SocketProvider } from '../../contexts/SocketContext';

describe('Truck component', () => {
  let wrapper;

  it('should render Truck component', () => {
    wrapper = shallow(
      <SocketProvider>
        <AppContextProvider initialState={{ containers: initialContainers }}>
          <Truck />
        </AppContextProvider>
      </SocketProvider>
    );
  });

  it('should render initial layout', () => {
    wrapper = shallow(
      <SocketProvider>
        <AppContextProvider initialState={{ containers: initialContainers }}>
          <Truck />
        </AppContextProvider>
      </SocketProvider>
    );
    expect(wrapper.getElements()).toMatchSnapshot();
  });

  it('should be able to click button', () => {
    // const spy = jest.spyOn(Truck.prototype, 'onShuffleClick');
    const mountedTruck = shallow(<Truck />);
    expect(mountedTruck.find('h3').text()).toContain('There is a beauty truck with 4 containers.');
    // mountedTruck.find('button').simulate('click');
    // expect(spy).toHaveBeenCalled();
  });
});
