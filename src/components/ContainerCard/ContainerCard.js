import React from 'react';
import Bottle from '../Bottle/Bottle';
import styles from './ContainerCard.scss';

const ContainerCard = ({ container }) => {
  const { id, min, max, temperature, bottles = [] } = container;
  const hasBottles = bottles && bottles.length > 0;
  const hasSensorValue = temperature !== undefined;

  const inDanger = React.useMemo(() => {
    return hasSensorValue && (temperature < min || temperature > max);
  }, [hasSensorValue, temperature, min, max]);

  return (
    <div className={styles.container} data-test-id="container">
      <div className={styles.header}>
        <h4>{`Container #${id}`}</h4>
      </div>
      <div className={styles.content}>
        {hasBottles ? (
          <>
            <p>
              Sensor: <span data-test-id="sensor">{hasSensorValue ? temperature : '---'}</span>°C
            </p>
            <p>
              Temperature range: {min}°C - {max}°C <br />
              <small>Set alert if only all bottles are in danger</small>
            </p>

            {hasSensorValue && (
              <div className={`${styles.alert} ${inDanger ? styles.danger : ''}`} data-test-id="alert">
                <span>{inDanger ? 'Alert!' : 'Ok!'}</span>
              </div>
            )}

            {bottles.map((bottle, index) => (
              <Bottle bottle={bottle} key={index} inDanger={inDanger} />
            ))}
          </>
        ) : (
          <h4>No bottles!</h4>
        )}
      </div>
    </div>
  );
};

export default ContainerCard;
