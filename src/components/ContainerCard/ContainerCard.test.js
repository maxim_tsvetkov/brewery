import React from 'react';
import { shallow } from 'enzyme';

import ContainerCard from './ContainerCard';

describe('ContainerCard component', () => {
  const emptyContainer = {
    id: 1,
    min: undefined,
    max: undefined,
    temperature: undefined,
    bottles: [],
  };

  const nokContainer = {
    id: 2,
    min: 4,
    max: 7,
    temperature: -2,
    bottles: [
      {
        name: 'Pilsner',
        min: 4,
        max: 6,
      },
      {
        name: 'IPA',
        min: 5,
        max: 6,
      },
      {
        name: 'Lager',
        min: 4,
        max: 7,
      },
    ],
  };

  const okContainer = {
    id: 3,
    min: 3,
    max: 8,
    temperature: 7,
    bottles: [
      {
        name: 'Stout',
        min: 6,
        max: 8,
      },
      {
        name: 'Wheat beer',
        min: 3,
        max: 5,
      },
      {
        name: 'Pale Ale',
        min: 4,
        max: 6,
      },
    ],
  };

  let container;

  it('should render ContainerCard component', () => {
    container = shallow(<ContainerCard container={emptyContainer} />);
  });

  it('should render empty container', () => {
    container = shallow(<ContainerCard container={emptyContainer} />);
    expect(container.getElements()).toMatchSnapshot('./__snapshots__/EmptyContainer.test.js.snap');
  });

  it('should render full container with a danger alert', () => {
    container = shallow(<ContainerCard container={nokContainer} />);
    expect(container.getElements()).toMatchSnapshot('./__snapshots__/FullNokContainer.test.js.snap');
  });

  it('should render full container in normal state', () => {
    container = shallow(<ContainerCard container={okContainer} />);
    expect(container.getElements()).toMatchSnapshot('./__snapshots__/FullOkContainer.test.js.snap');
  });
});
