import { shuffleBottles } from '../utils/util';
import { BeerTypes } from '../mockData/data';

export class BuildStateService {
  static getShuffleBottlesState(state) {
    return { ...state, containers: shuffleBottles(state.containers, BeerTypes) };
  }

  static getUpdateTemperatureState(state, { id, temperature }) {
    state.containers.forEach((container) => {
      if (container.id === Number(id)) {
        container.temperature = temperature;
      }
    });
    return { ...state, containers: state.containers };
  }
}
