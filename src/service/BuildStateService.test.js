import { BuildStateService } from './BuildStateService';

describe('BuildState Service', () => {
  let state = {
    containers: [],
  };

  describe('getShuffleBottlesState function', () => {
    it('should update state after shuffle', () => {
      expect(BuildStateService.getShuffleBottlesState(state).containers).toHaveLength(0);

      state = { containers: [{ id: 1, bottles: [] }] };
      expect(BuildStateService.getShuffleBottlesState(state).containers).toHaveLength(1);
      expect(BuildStateService.getShuffleBottlesState(state).containers[0].bottles).toHaveLength(3);
    });
  });

  describe('getUpdateTemperatureState function', () => {
    it('should invoke function without arguments', () => {
      state = { containers: [{ id: 1, temperature: 0 }] };
      expect(BuildStateService.getUpdateTemperatureState(state, { id: 1, temperature: 5 })).toStrictEqual({
        containers: [{ id: 1, temperature: 5 }],
      });
    });
  });
});
