export class WsService {
  constructor() {
    this.host = 'ws://127.0.0.1:8999';
    this.socket = null;
  }

  connect() {
    this.socket = new WebSocket(this.host);
    this.socket.onopen = () => {
      console.log(`Connected ${this.host}`);
    };

    this.socket.onerror = () => {
      console.log('Connection failed');
    };

    this.socket.onclose = () => {
      this.socket.close();
    };

    this.socket.onmessage = () => {
      console.log('Initialised');
    };
  }

  send({ type, data }) {
    if (type) {
      this.socket.send(JSON.stringify({ type, data }));
    }
  }
}
