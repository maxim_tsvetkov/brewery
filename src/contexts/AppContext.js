/* istanbul ignore file */
import React, { createContext, useReducer } from 'react';
import { BuildStateService } from '../service/BuildStateService';

const AppContext = createContext({});

const reducer = (state, action) => {
  switch (action.type) {
    case 'SHUFFLE_BOTTLES':
      return BuildStateService.getShuffleBottlesState(state);
    case 'UPDATE_TEMPERATURE':
      // eslint-disable-next-line
      let { id, temperature } = action;
      return BuildStateService.getUpdateTemperatureState(state, { id, temperature });
    default:
      return state;
  }
};

const AppContextProvider = ({ children, initialState }) => {
  const [state, dispatch] = useReducer(reducer, { ...initialState });

  return <AppContext.Provider value={{ state, dispatch }}>{children}</AppContext.Provider>;
};

export { AppContext, AppContextProvider };
