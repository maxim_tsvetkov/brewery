import React, { createContext, useState, useEffect } from 'react';
import { WsService } from '../service/WsService';

const SocketContext = createContext({});
const ws = new WsService();
ws.connect();

export const socketEvents = ({ setValue }) => {
  ws.socket.onmessage = (message) => {
    let jsonMessage;

    try {
      jsonMessage = JSON.parse(message.data);
    } catch (e) {
      throw `Invalid JSON: ${message.data}`;
    }

    const { type, data } = jsonMessage;

    switch (type) {
      case 'connection':
        console.log('Frontend connected');
        socketStopPolling();
        break;
      case 'updateSensorValue':
        setValue((state) => {
          return { ...state, type, data };
        });
        break;
      default:
    }
  };
};

export const socketRunPolling = ({ ids }) => {
  ws.send({ type: 'pollingSensorValue', data: { ids } });
};

export const socketStopPolling = () => {
  ws.send({ type: 'stopPolling', data: {} });
};

export const initSockets = ({ setValue }) => {
  socketEvents({ setValue });
};

const SocketProvider = (props) => {
  const [value, setValue] = useState({
    type: '',
    data: {},
  });

  useEffect(() => initSockets({ setValue }), [initSockets]);

  return <SocketContext.Provider value={value}>{props.children}</SocketContext.Provider>;
};

export { SocketContext, SocketProvider };
