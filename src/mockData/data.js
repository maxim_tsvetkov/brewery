export const BeerTypes = [
  {
    name: 'Pilsner',
    min: 4,
    max: 6,
  },
  {
    name: 'IPA',
    min: 5,
    max: 6,
  },
  {
    name: 'Lager',
    min: 4,
    max: 7,
  },
  {
    name: 'Stout',
    min: 6,
    max: 8,
  },
  {
    name: 'Wheat beer',
    min: 3,
    max: 5,
  },
  {
    name: 'Pale Ale',
    min: 4,
    max: 6,
  },
];

export const initialContainers = [
  {
    id: 1,
    min: null,
    max: null,
    temperature: null,
    bottles: [],
  },
  {
    id: 2,
    min: null,
    max: null,
    temperature: null,
    bottles: [],
  },
  {
    id: 3,
    min: null,
    max: null,
    temperature: null,
    bottles: [],
  },
  {
    id: 4,
    min: null,
    max: null,
    temperature: null,
    bottles: [],
  },
];
