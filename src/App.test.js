import React from 'react';
import { shallow } from 'enzyme';
import App from './App';
import Truck from './components/Truck/Truck';

describe('App component', () => {
  let app;

  it('should render App component', () => {
    app = shallow(<App />);
  });

  it('should contain an inner header and truck component', () => {
    app = shallow(<App />);

    expect(app.find('h1').text()).toContain("Shane's Brewery");
    expect(app.contains(<Truck />)).toBe(true);
  });
});
