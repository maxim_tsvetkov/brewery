import { shuffleBottles } from './util';
import { BeerTypes, initialContainers } from '../mockData/data';

describe('Utils function', () => {
  describe('shuffleBottles function', () => {
    it('should invoke function without arguments', () => {
      expect(shuffleBottles()).toStrictEqual([]);
    });

    it('should fill empty container with default number of bottles', () => {
      let emptyContainer = {
        id: 1,
        bottles: [],
        min: null,
        max: null,
      };

      const fulledContainers = shuffleBottles([emptyContainer], BeerTypes);
      expect(fulledContainers).toHaveLength(1);
      expect(fulledContainers[0].bottles).toHaveLength(3);
      expect(fulledContainers[0].min).not.toBeNull();
      expect(fulledContainers[0].max).not.toBeNull();
    });

    it('should fill set of containers', () => {
      const fulledContainers = shuffleBottles(initialContainers, BeerTypes, 1);
      expect(fulledContainers).toHaveLength(4);
      expect(fulledContainers[0].bottles).toHaveLength(1);
      expect(fulledContainers[1].bottles).toHaveLength(1);
      expect(fulledContainers[2].bottles).toHaveLength(1);
      expect(fulledContainers[3].bottles).toHaveLength(1);
      expect(fulledContainers[0].min).not.toBeNull();
      expect(fulledContainers[0].max).not.toBeNull();
    });
  });
});
