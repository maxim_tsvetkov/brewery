export const shuffleBottles = (containers = [], beers = [], maxBottles = 3) => {
  for (let it = 0; it < containers.length; it++) {
    containers[it].bottles = [];
    containers[it].min = null;
    containers[it].max = null;
    containers[it].temperature = null;
    for (let bottles = 0; bottles < maxBottles; bottles++) {
      const randomBottle = beers[Math.floor(Math.random() * Math.floor(beers.length))];
      containers[it].bottles.push(randomBottle);
      containers[it].min =
        randomBottle.min < containers[it].min || containers[it].min === null ? randomBottle.min : containers[it].min;
      containers[it].max = randomBottle.max > containers[it].max ? randomBottle.max : containers[it].max;
    }
  }

  return containers;
};
