const express = require('express');
const http = require('http');
const https = require('https');
const WebSocket = require('ws');

const app = express();

const server = http.createServer(app);
const wss = new WebSocket.Server({ server });

// Initial state
const API_URL = 'https://temperature-sensor-service.herokuapp.com/sensor';
const TIME_POLLING_INTERVAL_SEC = 5;
let timeoutId = undefined;
let ids = [];
let wsInstance = undefined;

const containersMockData = [
  {
    id: 1,
    temperature: -1,
  },
  {
    id: 2,
    temperature: 5,
  },
  {
    id: 3,
    temperature: 9,
  },
  {
    id: 4,
    temperature: undefined,
  },
];

function _delay() {
  // eslint-disable-next-line
  return new Promise((resolve) => {
    timeoutId = setTimeout(resolve, 1000 * TIME_POLLING_INTERVAL_SEC);
  });
}

function polledMethod() {
  ids.forEach((id) => {
    console.log(`${API_URL}/${id}`);

    if (process.env.NODE_ENV === 'prod') {
      try {
        https
          .get(`${API_URL}/${id}`, (resp) => {
            let data = '';

            resp.on('data', (chunk) => {
              data += chunk;
            });

            resp.on('end', () => {
              const response = JSON.parse(data);
              const { id, temperature } = response;
              wsInstance.send(JSON.stringify({ type: 'updateSensorValue', data: { id, temperature } }));
            });
          })
          .on('error', (err) => {
            console.log('Error: ' + err.message);
          });
      } catch (e) {
        throw 'Request error';
      }
    }

    // emulation
    if (process.env.NODE_ENV === 'test') {
      containersMockData.forEach(({ id, temperature }) => {
        wsInstance.send(JSON.stringify({ type: 'updateSensorValue', data: { id, temperature } }));
      });
    }
  });
}

function clearPolling() {
  clearTimeout(timeoutId);
  timeoutId = undefined;
}

function polling(methodToBePolled) {
  clearPolling();
  _delay().then(() => {
    if (timeoutId !== undefined) {
      methodToBePolled();
      polling(methodToBePolled);
    }
  });
}

// WS callbacks
wss.on('connection', (ws) => {
  ws.on('message', (message) => {
    let jsonMessage;

    try {
      jsonMessage = JSON.parse(message);
    } catch (e) {
      throw `On message: Invalid JSON`;
    }

    // Handler
    const { type, data } = jsonMessage;
    switch (type) {
      case 'pollingSensorValue':
        clearPolling();
        ids = data.ids.split(',') || [];
        polledMethod();
        polling(polledMethod.bind(this));
        break;
      case 'stopPolling':
        ids = [];
        clearPolling();
        break;
      default:
    }
  });

  //send immediatly a feedback to the incoming connection
  ws.send(JSON.stringify({ type: 'connection', data: { message: 'Connected' } }));
  wsInstance = ws;
});

//start our server
server.listen(8999, () => {
  console.log(`Env: ${process.env.NODE_ENV}. Server started on port ${server.address().port}`);
});
