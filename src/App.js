import React from 'react';
import Truck from './components/Truck/Truck';
import { SocketProvider } from './contexts/SocketContext';
import { AppContextProvider } from './contexts/AppContext';
import { initialContainers } from './mockData/data';
import styles from './App.scss';

const App = () => {
  return (
    <SocketProvider>
      <AppContextProvider initialState={{ containers: initialContainers }}>
        <div className={styles.wrapper}>
          <h1>Shane&apos;s Brewery</h1>
          <Truck />
        </div>
      </AppContextProvider>
    </SocketProvider>
  );
};

export default App;
